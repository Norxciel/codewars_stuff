# Codewars - On a road to improve myself

## What's this?

As a developper, I try each day to be a little bit better at what I'm doing. I found [Codewars][0] to have some interesting challenges which I'll try to solve through my skills in Python and Javascript.

So basically: I'll keep track on what I solve on this website in this project.

[0]: https://www.codewars.com/
