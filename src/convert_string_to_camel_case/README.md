# Convert string to camel case

## What's this?

Complete the method/function so that it converts dash/underscore delimited words into camel casing. The first word within the output should be capitalized only if the original word was capitalized (known as Upper Camel Case, also often referred to as Pascal case). The next words should be always capitalized.

> Quick example:
> "the-stealth-warrior" gets converted to "theStealthWarrior"
> "The_Stealth_Warrior" gets converted to "TheStealthWarrior"
> "The_Stealth-Warrior" gets converted to "TheStealthWarrior"

## What's the plan?

First, being sure that user enter an appropriate number. Then, enumerate this number, adding it up into a string, and track its length. when string length is matching user entry, we got our page number.

source: [Codewars][0]

[0]: https://www.codewars.com/kata/517abf86da9663f1d2000003
