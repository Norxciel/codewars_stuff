import pytest

import convert_string_to_camel_case.convert_string_to_camel_case_src.convert_to_camel as convert_to_camel


@pytest.mark.parametrize(
    ('param', 'expected'),
    (
        ("", ""),
        ("the-stealth-warrior", "theStealthWarrior"),
        ("The_Stealth_Warrior", "TheStealthWarrior"),
        ("The_Stealth-Warrior", "TheStealthWarrior"),
        ("The_Stealth-Warrior", "TheStealthWarrior"),
        ("A-B-C", "ABC"),
    )
)
def test_should_work(param, expected):
    assert convert_to_camel.convert_to_camel(param) == expected
