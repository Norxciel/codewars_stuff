# coding:utf-8


def convert_to_camel(text: str) -> str:
    import re

    if len(text) == 0:
        return ""

    splited = re.split("-|_", text)

    res = ''.join([x for x in splited[0]]+[x.capitalize()
                  for x in splited[1:]])

    return res


if __name__ == '__main__':
    print(convert_to_camel("the-stealth-warrior"))
