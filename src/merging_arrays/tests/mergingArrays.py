import pytest as pt

from src.mergingArrays import merge_arrays

test_data = [
    ([1,2,3,4], [5,6,7,8], [1,2,3,4,5,6,7,8]),
    ([1,3,5,7,9], [10,8,6,4,2], [1,2,3,4,5,6,7,8,9,10]),
    ([1,3,5,7,9,11,12], [1,2,3,4,5,10,12], [1,2,3,4,5,7,9,10,11,12]),
    ([-20, -10, -5, 0, 6], [10, 25, 35, 36, 37, 38, 39], [-20, -10, -5, 0, 6, 10, 25, 35, 36, 37, 38, 39])
]

test_types_error = [
    ("oui", 12),
    ({},True)
]

@pt.mark.parametrize(
    'arr1, arr2, expectation', test_data, ids=['base', 'even_odd-reverse', 'duplicates', 'with_negatives']
)
def test_merge(arr1, arr2, expectation):
    assert merge_arrays(arr1, arr2) == expectation


@pt.mark.parametrize(
    'arr1, arr2', test_types_error
)
def test_raising_exception(arr1, arr2):
    with pt.raises(TypeError):
        merge_arrays(arr1, arr2)