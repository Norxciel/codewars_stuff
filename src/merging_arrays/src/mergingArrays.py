def merge_arrays(arr1:list, arr2:list)->list:
    """ Merge 2 arrays together.    
    """

    # If params not list-type
    if not isinstance(arr1, list) or not isinstance(arr2, list):
        # Raise Error
        raise TypeError("Parameters must be list-type")
    
    # Setup return variable
    res = None

    # Set res to copy of arr1
    res = arr1[:]
    # Extends res with arr2 values
    res.extend(arr2)

    # Using set to handle duplicates
    res = list(set(res))
    # Sort res (ascending)
    res.sort()
    

    # Return res
    return res

if __name__ == '__main__':
    print("Merging [1,2,3] and [4,5,6]")
    print(merge_arrays([1,2,3], [4,5,6]))