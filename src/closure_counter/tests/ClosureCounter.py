import pytest

from src.ClosureCounter import main


def test_main_is_func():
    assert type(main).__name__, 'function'

def test_return_1_when_invoke():
    assert main()() == 1

def test_should_increment_each_time():
    counter_function = main()

    assert counter_function() == 1
    assert counter_function() == 2

def test_multi_instance():
    counter_one = main()
    counter_two = main()

    assert counter_one() == 1
    assert counter_one() == 2

    assert counter_two() == 1
    assert counter_two() == 2