def main():
    """ Will return function to increase local variable.
    """

    # Setup local var
    a = 0

    def modif():
        """Set outer variable a to itself + 1"""

        # Define outer variable a
        nonlocal a

        # Add one
        a = a + 1

        # Return new value
        return a

    # Return function to call
    return modif

if __name__ == '__main__':
    counter = main()

    print("Initial counter is 1")
    print(counter())

    print("Counter is now 2")
    print(counter())

    print("Counter is now 3")
    print(counter())