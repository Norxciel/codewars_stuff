import pytest

import scramblies.scramblies_src.scramblies as scramblies


# @pytest.mark.skip("To implement")
@pytest.mark.parametrize(
    ('param1', 'param2', 'expected'),
    (
        ('rkqodlw', 'world', True),
        ('cedewaraaossoqqyt', 'codewars', True),
        ('katas', 'steak', False),
        ('scriptjava', 'javascript', True),
        ('scriptingjava', 'javascript', True)
    )
)
def test_should_work(param1, param2, expected):
    assert scramblies.main(param1, param2) == expected

@pytest.mark.parametrize(
    ('param1', 'param2', 'expected'),
    (
        ('rkqodlw', 'world', True),
        ('cedewaraaossoqqyt', 'codewars', True),
        ('katas', 'steak', False),
        ('scriptjava', 'javascript', True),
        ('scriptingjava', 'javascript', True)
    )
)
def test_alternative_should_work(param1, param2, expected):
    assert scramblies.alternative(param1, param2) == expected
