def main(text1: str, text2: str) -> bool:
    # get letters and their counts from text1
    letters = {}    
    for letter in text1:
        if letter in letters:
            letters[letter] += 1
        else:
            letters[letter] = 1
    
    # check if text2 can be made from text1
    for letter in text2:
        if letter in letters and letters[letter] > 0:
            letters[letter] -= 1
        else:
            return False
    
    return True

def alternative(text1: str, text2: str) -> bool:
    for char in set(text2):
        if text1.count(char) < text2.count(char):
            return False
    return True

if __name__ == "__main__":
    # print(main('rkqodlw', 'world'))  # False
    alternative('rkqodlw', 'world')
