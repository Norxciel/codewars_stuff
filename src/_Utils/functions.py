import os

def new_project(project_name: str = None):
    if not project_name:
        project_name = input("Nom du projet: ")
        if len(project_name)<1:
            raise ValueError("Project name length must be > 1!")

    if os.path.exists(f"./src/{project_name}"):
        raise KeyError(f"Project {project_name} already exists!")
    
    os.makedirs(f"./src/{project_name}")

    SRC_DIR_FILEPATH: str = f"./src/{project_name}/{project_name}_src"
    SRC_FILEPATH: str = f"./src/{project_name}/{project_name}_src/{project_name}.py"
    TEST_DIR_FILEPATH: str = f"./src/{project_name}/{project_name}_test"
    TEST_FILEPATH: str = f"./src/{project_name}/{project_name}_test/{project_name}_tests.py"

    os.makedirs(SRC_DIR_FILEPATH)
    os.makedirs(TEST_DIR_FILEPATH)

    with open(SRC_FILEPATH, 'w') as file:
        file.writelines("""def main(message: str) -> str:
    ...

if __name__ == "__main__":
    main()                        
""")
        
    with open(TEST_FILEPATH, 'w') as file:
        file.writelines(f"""import pytest
import {project_name}.{project_name}_src.{project_name} as {project_name}

@pytest.mark.parametrize(
    ('param1', 'expected'),
    (
        ('param1_1', 'expected_1'),
    )
)
def test_should_work(param1, expected):
    assert {project_name}.main(param1) == expected
""")
        
if __name__ == "__main__":
    new_project()
