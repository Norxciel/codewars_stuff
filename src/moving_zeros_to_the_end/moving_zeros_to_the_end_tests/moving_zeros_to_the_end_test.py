import pytest

import moving_zeros_to_the_end.moving_zeros_to_the_end_src.moving_zeros_to_the_end as moving_zeros_to_the_end


# @pytest.mark.skip("To implement")
@pytest.mark.parametrize(
    ('param1', 'expected'),
    (
        ([1, 0, 1, 2, 0, 1, 3], [1, 1, 2, 1, 3, 0, 0]),
    )
)
def test_should_work(param1, expected):
    assert moving_zeros_to_the_end.main(param1) == expected
