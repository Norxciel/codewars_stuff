def main(lst: list[int],) -> list[int]:
    # Get list of zeros from original list
    zeros = [0 for _ in range(lst.count(0))]
    # Return list of non-zero element, with zeros list at the end
    return [x for x in lst if x != 0] + zeros


if __name__ == "__main__":
    print(main([1, 0, 1, 2, 0, 1, 3]))  # [1, 1, 2, 1, 3, 0, 0]
