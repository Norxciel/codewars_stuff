# Split Strings

## Objective

Write an algorithm that takes an array and moves all of the zeros to the end, preserving the order of the other elements.

source: [Codewars][0]

[0]: https://www.codewars.com/kata/52597aa56021e91c93000cb0
