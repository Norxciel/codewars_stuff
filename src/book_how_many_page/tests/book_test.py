import pytest

import book_how_many_page.book_how_many_page.book as book


@pytest.mark.parametrize(
    ('summary', 'pages'),
    (
        (25, 17),
        (1, 1),
    ),
)
def test_summary(summary, pages):
    assert book.guess_pages_from(summary) == pages


@pytest.mark.parametrize(
    ('summary'),
    (
        (1.50),
        ("test"),
        ([])
    )
)
def test_raising_error_when_summary_not_int(summary):
    with pytest.raises(TypeError):
        book.guess_pages_from(summary)
