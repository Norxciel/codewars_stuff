# How many pages in a book?

## What's this?

Imagine a book, from page 1 to page N. Now imagine this thing call **Summary**, which is all number of digit of all page numbers.

Give Summary, get N.

> Quick example:
> If summary -> 25, then 1234567891011121314151617 is all the digits.
> Thus, N -> 17.

## What's the plan?

First, being sure that user enter an appropriate number. Then, enumerate this number, adding it up into a string, and track its length. when string length is matching user entry, we got our page number.

source: [Codewars][0]

[0]: https://codewars.com/kata/622de76d28bf330057cd6af8
