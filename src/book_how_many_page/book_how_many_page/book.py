# coding:utf-8



def guess_pages_from(summary:int=25)->int:
    if not isinstance(summary, int):
        raise TypeError("Summary must be Integer")

    string = ''

    for i in range(summary):
        string += str(i+1)

        if len(string)>=summary:
            return i+1

    return None

if __name__ == '__main__':
    print(guess_pages_from(100000))