def main(text: str) -> list[str]:
    # if uneven...
    if len(text) % 2 != 0:
        # ...add underscore at the end
        text += "_"

    # return list of 2 characters
    return [text[i:i + 2] for i in range(0, len(text), 2)]


if __name__ == "__main__":
    # print(main("abcdef"))  # ['ab', 'cd', 'ef']
    print(main("abc"))  # ['ab', 'c_']
