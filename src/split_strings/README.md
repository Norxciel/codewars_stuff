# Split Strings

## What's this?

Complete the solution so that it splits the string into pairs of two characters. If the string contains an odd number of characters then it should replace the missing second character of the final pair with an underscore ('_').



> Quick example:
> 'abc' =>  ['ab', 'c_']
> 'abcdef' => ['ab', 'cd', 'ef']

## What's the plan?

Just do it you silly goose!

source: [Codewars][0]

[0]: https://www.codewars.com/kata/515de9ae9dcfc28eb6000001
