import pytest

import split_strings.split_strings_src.split_strings as split_strings


# @pytest.mark.skip("To implement")
@pytest.mark.parametrize(
    ('param', 'expected'),
    (
        ("abc", ['ab', 'c_']),
        ("abcdef", ['ab', 'cd', 'ef']),
        ("", []),
        ("x", ["x_"]),
    )
)
def test_should_work(param, expected):
    assert split_strings.main(param) == expected
