import pytest

import rot13.rot13_src.rot13 as rot13


@pytest.mark.parametrize(
    ('param1', 'expected'),
    (
        ('EBG13 rknzcyr.', 'ROT13 example.'),
        ('123', '123'),
        ('Guvf vf npghnyyl gur svefg xngn V rire znqr. Gunaxf sbe svavfuvat vg! :)', 'This is actually the first kata I ever made. Thanks for finishing it! :)'),
        ('@[`{', '@[`{'),
        ("How can you tell an extrovert from an\nintrovert at NSA? Va gur ryringbef,\ngur rkgebireg ybbxf ng gur BGURE thl'f fubrf.", "Ubj pna lbh gryy na rkgebireg sebz na\nvagebireg ng AFN? In the elevators,\nthe extrovert looks at the OTHER guy's shoes."),
    )
)
def test_should_work(param1, expected):
    assert rot13.main(param1) == expected