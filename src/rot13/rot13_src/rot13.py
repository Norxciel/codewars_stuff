import regex as re
def main(message: str) -> str:
    L: list[str] = list("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
    l: list[str] = list("abcdefghijklmnopqrstuvwxyz")

    ret: str = ""

    for let in message:
        if not let.isalpha():
            ret += let
            continue

        ret += L[(L.index(let)+13)%26] if let.isupper() else l[(l.index(let)+13)%26]

    return ret

if __name__ == "__main__":
    print(main("EBG13 rknzcyr"))