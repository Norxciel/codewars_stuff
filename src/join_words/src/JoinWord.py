def main(words:list[str])->str:
    """"""

    # If words not list-type
    if not isinstance(words, list):
        # Raising TypeError
        raise TypeError("Parameter must be list-type")
    
    # If words empty
    if len(words)==0:
        # Raising ValueError
        raise ValueError("List must not be empty")

    # Initializing variable
    res = None

    # Setup first word to his first letter capitalized + its other letters
    words[0] = words[0][0].capitalize()+words[0][1:]

    # Joining each words from list with space
    res = ' '.join(words)
    res+='.' # Add period at the end.

    # Returning result
    return res

if __name__ == '__main__':
    print("Joining ['i', 'am', 'an', 'AI'] to a sentence.")
    print(main(["i", "am", "an", "AI"]))