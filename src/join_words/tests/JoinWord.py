import pytest as pt
from src.JoinWord import main

data_test = (
    (["i", "am", "an", "AI"], "I am an AI."),
    (["FIELDS","of","CORN","are","to","be","sown"], "FIELDS of CORN are to be sown."),
    (["i'm","afraid","I","can't","let","you","do","that"], 	"I'm afraid I can't let you do that.")
)

test_words_not_list = (
    (""),
    ({}),
    (True),
    (12),
    (5.5),
)

@pt.mark.parametrize(
    "words, expect", data_test
)
def test_main(words, expect):
    assert main(words) == expect


@pt.mark.parametrize(
    "param", test_words_not_list
)
def test_raising_error_when_words_not_list(param):
    with pt.raises(TypeError):
        main(param)

def test_error_when_empty_list():
    with pt.raises(ValueError):
        main([])