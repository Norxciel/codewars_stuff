import pytest

import pick_peaks.pick_peaks_src.pick_peaks as pick_peaks


# @pytest.mark.skip("To implement")
@pytest.mark.parametrize(
    ('param1', 'expected'),
    (
        ([0, 1, 2, 5, 1, 0], {"pos": [3], "peaks": [5]}),
        ([3, 2, 3, 6, 4, 1, 2, 3, 2, 1, 2, 3],
         {"pos": [3, 7], "peaks": [6, 3]}),
        ([1, 2, 2, 2, 1], {"pos": [1], "peaks": [2]}),
        ([1, 2, 2, 2, 2], {"pos": [], "peaks": []}),
        ([], {"pos": [], "peaks": []}),
        ([18, 18, 10, -3, -4, 15, 15, -1, 13, 17, 11, 4, 18, -4, 19, 4, 18, 10, -4, 8, 13, 9, 16,
         18, 6, 7], {'pos': [5, 9, 12, 14, 16, 20, 23], 'peaks': [15, 17, 18, 19, 18, 13, 18]})
    )
)
def test_should_work(param1, expected):
    assert pick_peaks.main(param1) == expected
