def main(arr: list[int],) -> list[int]:
    # setting return structure
    dictReturn: dict[str, list[int]] = {"pos": [], "peaks": []}

    # if arr is empty, return empty return dict
    if len(arr) == 0:
        return dictReturn

    # iterate through arr
    for i in range(1, len(arr) - 1):
        # if element is greater than previous one
        if arr[i] > arr[i - 1]:
            # iter through rest of arr
            for j in range(i + 1, len(arr)):
                # if element is lesser than next one
                if arr[i] < arr[j]:
                    # leave second loop
                    break
                # if element is greater than next one
                if arr[i] > arr[j]:
                    # add position and value to return dict
                    dictReturn["pos"].append(i)
                    dictReturn["peaks"].append(arr[i])
                    # leave second loop
                    break

    return dictReturn


if __name__ == "__main__":
    print(main([0, 1, 2, 5, 1, 0]))  # {"pos": [3], "peaks": [5]}
