# coding:utf-8

CONVERT_DICT: dict[str:int] = {
    "M": 1000,
    "D": 500,
    "C": 100,
    "L": 50,
    "X": 10,
    "V": 5,
    "I": 1
}


def main(romanNum: str) -> str:
    total: int = 0
    prev: int = None

    for letter in romanNum:
        n: int = CONVERT_DICT.get(letter)
        total += n if prev is None or prev >= n else n - 2 * prev
        prev = n

    return total


if __name__ == '__main__':
    param: str = "MM"  # 2000
    print(main(param))
