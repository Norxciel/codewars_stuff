import pytest

import roman_numerals_decoder.roman_numerals_decoder_src.roman_numerals_decoder as roman_numerals_decoder


# @pytest.mark.skip("To implement")
@pytest.mark.parametrize(
    ('param', 'expected'),
    (
        ("MM", 2000),
        ("MDCLXVI", 1666),
        ("M", 1000),
        ("CD", 400),
        ("XC", 90),
        ("XL", 40),
        ("I", 1),
    )
)
def test_should_work(param, expected):
    assert roman_numerals_decoder.main(param) == expected
