import pytest

import src.SumDigits as sd

@pytest.mark.parametrize(
    ('number', 'expected'),
    (
        (16, 7),
        (942, 6),
        (132189, 6),
        (493193, 2),
    )
)
def test_numbers(number:int, expected:int):
    assert sd.sum_digits_from(number) == expected

@pytest.mark.parametrize(
    ('number'),
    (
        (1.5),
        ("test"),
        ([]),
        ({}),
    )
)
def test_raising_TypeError_when_number_not_int(number):
    with pytest.raises(TypeError):
        sd.sum_digits_from(number)