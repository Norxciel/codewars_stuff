# Sum of Digits - Digital Root

## What's this?

For a gave number, we add it's digits all together.
If it's a 2-digits or more number, we repeat the process until 1-digit number.

## What's the plan?

Reccursively iterate through first number entered.

source: [Codewars][0]

[0]: https://www.codewars.com/kata/541c8630095125aba6000c00
