def sum_digits_from(number:int=1)->int:
    """"""

    # If number not instance of Integer
    if not isinstance(number, int):
        # raising TypeError
        raise TypeError("Number must be an Integer")

    # If len of stringify number is 1
    if len(str(number))==1:
        # return result
        return number
    else:
        # Setting up sum variable to 0
        sum = 0

        # Iterating through stringify number
        for l in str(number):
            # Add casted-to-int digit to sum
            sum += int(l)

        # call sum_digits_from with new parameter
        return sum_digits_from(sum)

if __name__ == '__main__':
    print(sum_digits_from(19))