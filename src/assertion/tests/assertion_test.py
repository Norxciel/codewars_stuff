import unittest as ut

from assertion.assertion.assertion import PhoneBook


class TestPhoneBook(ut.TestCase):

    def setUp(self):
        self.phone_book = PhoneBook()

    def test_create_method(self):
        self.phone_book.create('User', '1234-5678')

        self.assertEqual(self.phone_book['User'], '1234-5678')

    def test_retrieve_method_when_the_name_exists(self):
        self.phone_book.create('User', '1234-5678')

        self.assertEqual(self.phone_book.retrieve('User'), '1234-5678')

    def test_retrieve_method_when_the_name_does_not_exist(self):
        with self.assertRaises(KeyError):
            self.phone_book.retrieve('User')

    def test_update_method_when_the_name_exists(self):
        self.phone_book.create('User', '1234-5678')
        self.phone_book.update('User', '123')

        self.assertEqual(self.phone_book['User'], '123')

    def test_delete_method_when_the_name_exists(self):
        self.phone_book.create('User', '1234-5678')
        self.phone_book.delete('User')

        self.assertFalse('User' in self.phone_book)


if __name__ == '__main__':
    ut.main()
