from collections import UserDict


class PhoneBook(UserDict):

    def create(self, name: str, phone_number: str) -> None:
        self[name] = phone_number  # Statement 1

    def retrieve(self, name: str) -> str:
        if name not in self:
            raise KeyError  # Statement 2

        return self[name]  # Statement 3

    def update(self, name: str, phone_number: str) -> None:
        if name not in self:
            raise KeyError

        self[name] = phone_number  # Statement 4

    def delete(self, name: str) -> None:
        if name not in self:
            raise KeyError

        del self[name]  # Statement 5